/**
 * This file is part of Hercules.
 * http://herc.ws - http://github.com/HerculesWS/Hercules
 *
 * Copyright (C) 2012-2018  Hercules Dev Team
 * Copyright (C)  Athena Dev Teams
 *
 * Hercules is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MAP_STORAGE_H
#define MAP_STORAGE_H

#include "common/hercules.h"
#include "common/db.h"

struct config_setting_t;
struct guild_storage;
struct item;
struct map_session_data;
struct storage_settings;
struct storage_data;

/**
 * Acceptable values for map_session_data.state.storage_flag
 */
enum storage_flag {
	STORAGE_FLAG_CLOSED = 0, // Closed
	STORAGE_FLAG_NORMAL = 1, // Normal Storage open
	STORAGE_FLAG_GUILD  = 2, // Guild Storage open
};

// Storage Access Modes [Smokexyz/Hercules]
enum storage_access_modes {
	STORAGE_ACCESS_VIEW = 0x0,
	STORAGE_ACCESS_GET  = 0x1,
	STORAGE_ACCESS_PUT  = 0x2,
	STORAGE_ACCESS_ALL  = STORAGE_ACCESS_VIEW | STORAGE_ACCESS_GET | STORAGE_ACCESS_PUT
};

struct storage_interface {
	VECTOR_DECL(struct storage_settings) configuration;
	void (*init) (bool minimal);
	void (*final) (void);
	/* */
	void (*reconnect) (void);
	bool (*config_read) (const char *filename, bool imported);
	void (*config_read_additional_fields) (struct config_setting_t *t, struct storage_settings *s_conf, const char *filename);
	/* */
	int (*get_id_by_name) (const char *storage_name);
	struct storage_data *(*ensure) (struct map_session_data *sd, int storage_id);
	const struct storage_settings *(*get_settings) (int storage_id);
	int (*delitem) (struct map_session_data *sd, struct storage_data *stor, int n, int amount);
	int (*open) (struct map_session_data *sd, struct storage_data *stor);
	int (*add) (struct map_session_data *sd, struct storage_data *stor, int index, int amount);
	int (*get) (struct map_session_data *sd, struct storage_data *stor, int index, int amount);
	int (*additem) (struct map_session_data *sd, struct storage_data *stor, struct item* item_data, int amount);
	int (*addfromcart) (struct map_session_data *sd, struct storage_data *stor, int index,int amount);
	int (*gettocart) (struct map_session_data *sd, struct storage_data *stor, int index,int amount);
	void (*close) (struct map_session_data *sd);
	void (*pc_quit) (struct map_session_data *sd, int flag);
	int (*comp_item) (const void *i1_, const void *i2_);
	void (*sortitem) (struct item *items, unsigned int size);
	int (*reconnect_sub) (union DBKey key, struct DBData *data, va_list ap);
};

struct guild_storage_interface {
	struct DBMap *db; // int guild_id -> struct guild_storage*
	/* */
	struct guild_storage *(*ensure) (int guild_id);
	/* */
	void (*init) (bool minimal);
	void (*final) (void);
	/* */
	int (*delete) (int guild_id);
	int (*open) (struct map_session_data *sd);
	int (*additem) (struct map_session_data *sd,struct guild_storage *stor,struct item *item_data,int amount);
	int (*delitem) (struct map_session_data *sd,struct guild_storage *stor,int n,int amount);
	int (*add) (struct map_session_data *sd,int index,int amount);
	int (*get) (struct map_session_data *sd,int index,int amount);
	int (*addfromcart) (struct map_session_data *sd,int index,int amount);
	int (*gettocart) (struct map_session_data *sd,int index,int amount);
	int (*close) (struct map_session_data *sd);
	int (*pc_quit) (struct map_session_data *sd,int flag);
	int (*save) (int account_id, int guild_id, int flag);
	int (*saved) (int guild_id); //Ack from char server that guild store was saved.
	struct DBData (*create) (union DBKey key, va_list args);
};

#ifdef HERCULES_CORE
void storage_defaults(void);
void gstorage_defaults(void);
#endif // HERCULES_CORE

HPShared struct storage_interface *storage;
HPShared struct guild_storage_interface *gstorage;

// [TMW2] Checks if two items are compatible enough to stack.
bool are_stackable(const struct item * const a,
                   const struct item * const b);

#endif /* MAP_STORAGE_H */
